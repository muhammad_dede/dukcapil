$(document).ready(function () {
    // Autoscroll bottom
    $(".chat_body").scrollTop($(".chat_body")[0].scrollHeight);

    // resfresh chat function
    function refreshChat() {
        $.ajax({
            url: window.location.href,
            type: "GET",
        }).done(function (response) {
            if (response.html == " ") {
                return;
            }
		$(".chat_body").empty();
            $(".chat_body").append(response.html);
            $(".chat_body").scrollTop($(".chat_body")[0].scrollHeight);
        }).fail(function (jqXHR, ajaxOptions, thrownError) {
            alert("Server Not Responding...");
        });
    }

    // Send Chat
    $(".btn_send_chat").on("click", function (event) {
        event.preventDefault();
        var form = $('.form_chat'),
            url = form.attr('action'),
            method = 'POST';
        $.ajax({
            url: url,
            method: method,
            data: form.serialize(),
            success: function (response) {
                if (response.status == 200) {
		    refreshChat();
                    $("#body").val("");
                }
            }
        });
    });

    // Get Chat form pusher
    var pusher = new Pusher('78256e98206ec9b69fb1', {
        cluster: 'ap1'
    });
    var channel = pusher.subscribe('chat-admin-channel');
    channel.bind('chat-admin-event', function (response) {
        refreshChat();
    });
});
