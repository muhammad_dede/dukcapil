<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
    use HasFactory;

    protected $table = 'chat_message';
    protected $primaryKey = 'id';

    protected $guarded = [];

    public function chat()
    {
        return $this->hasMany(Chat::class, 'id_chat', 'id');
    }
}
