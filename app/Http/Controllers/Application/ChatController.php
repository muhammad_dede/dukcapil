<?php

namespace App\Http\Controllers\Application;

use App\Events\SendChat;
use App\Http\Controllers\Controller;
use App\Models\Chat;
use App\Models\ChatMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ChatController extends Controller
{
    public function index(Request $request)
    {
        $chat = Chat::with('chatMessage')->where('id_pelapor', auth()->id())->where('session_end', null)->first();

        if ($request->ajax()) {
            $view = view('app.chat.chat-body', compact('chat'))->render();
            return response()->json(['html' => $view]);
        }

        return view('app.chat.index', compact('chat'));
    }

    public function send(Request $request)
    {
        if ($request->body) {

            $chat = Chat::with('chatMessage')->where('id_pelapor', auth()->id())->where('session_end', null)->first();

            if ($chat) {
                ChatMessage::where('id_chat', $chat->id)->where('is_pelapor', false)->update([
                    'last_seen' => now(),
                ]);
                $chat_message = ChatMessage::create([
                    'id_chat' => $chat->id,
                    'is_pelapor' => true,
                    'body' => $request->body,
                    'last_seen' => null,
                ]);
            } else {
                $chat = Chat::create([
                    'id' => time(),
                    'id_pelapor' => auth()->id(),
                    'id_user' => null,
                    'session_start' => now(),
                    'session_end' => null,
                ]);
                $chat_message = ChatMessage::create([
                    'id_chat' => $chat->id,
                    'is_pelapor' => true,
                    'body' => $request->body,
                    'last_seen' => null,
                ]);
            }

            event(new SendChat($chat_message));

            return response()->json(['status' => 200, 'message' => 'Chat terkirim']);
        } else {
            return response()->json(['status' => 400, 'message' => 'Required']);
        }
    }
}
