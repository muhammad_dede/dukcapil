@if ($chat)
    @foreach ($chat->chatMessage as $message)
        @if ($message->is_pelapor == false)
            <div class="d-flex justify-content-start mb-10 chat_message" id="chat_message_{{ $message->id }}">
                <div class="d-flex flex-column align-items-start">
                    <div class="p-5 rounded bg-light-info text-dark fw-bold mw-lg-400px text-start">
                        {{ $message->body }} <br>
                        <small
                            class="text-muted">{{ \Carbon\Carbon::parse($message->created_at)->isoFormat('H:mm') }}</small>
                    </div>
                </div>
            </div>
        @else
            <div class="d-flex justify-content-end mb-10 chat_message" id="chat_message_{{ $message->id }}">
                <div class="d-flex flex-column align-items-end">
                    <div class="p-5 rounded bg-light-primary text-dark fw-bold mw-lg-400px text-end">
                        {{ $message->body }} <br>
                        <small
                            class="text-muted">{{ \Carbon\Carbon::parse($message->created_at)->isoFormat('H:mm') }}</small>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@else
    <div class="d-flex justify-content-start mb-10 chat_message" id="chat_message_0">
        <div class="d-flex flex-column align-items-start">
            <div class="p-5 rounded bg-light-info text-dark fw-bold mw-lg-400px text-start">
                Hai
                saya Petugas Layanan Dukcapil Kota Cilegon, ada yang bisa kami bantu? <br>
                <small class="text-muted">{{ \Carbon\Carbon::parse(now())->diffForHumans() }}</small>
            </div>
        </div>
    </div>
@endif
