@extends('layouts.app.template')
@section('content')
    <div class="toolbar py-5 py-lg-15" id="kt_toolbar">
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <div class="page-title d-flex flex-column me-3">
                <h1 class="d-flex text-white fw-bolder my-1 fs-3">Live Chat</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                    <li class="breadcrumb-item text-white opacity-75">
                        <a href="{{ url('/') }}" class="text-white text-hover-primary">Beranda</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-white opacity-75 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-white opacity-75">Chat</li>
                </ul>
            </div>
        </div>
    </div>
    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <div class="content flex-row-fluid" id="kt_content">
            <div class="d-flex flex-column flex-lg-row">
                <div class="flex-column flex-lg-row-auto w-100 w-lg-200px w-xl-300px mb-10 mb-lg-0 text-center">
                    <img src="{{ asset('images/chat/icon-page-livechat.png') }}" alt="chat-logo">
                </div>
                <div class="flex-lg-row-fluid ms-lg-7 ms-xl-10">
                    <div class="card shadow" id="kt_chat_messenger">
                        <div class="card-header" id="chat_header">
                            <div class="card-title">
                                <div class="d-flex justify-content-center flex-column me-3">
                                    <span class="fs-4 fw-bolder text-gray-900 me-1 mb-2 lh-1">Dukcapil
                                        Cilegon</span>
                                    <div class="mb-0 lh-1">
                                        <span class="fs-7 fw-bold text-muted">Contact
                                            Center</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="scroll-y me-n5 pe-5 h-500px h-lg-auto overflow-scroll chat_body"
                                data-kt-scroll="true" data-kt-scroll-max-height="auto"
                                data-kt-scroll-dependencies="#kt_header, #kt_toolbar, #kt_footer, #kt_chat_messenger_header, #kt_chat_messenger_footer"
                                data-kt-scroll-wrappers="#kt_content, #kt_chat_messenger_body" data-kt-scroll-offset="-2px">
                                @include('app.chat.chat-body')
                            </div>
                        </div>
                        <div class="card-footer pt-4" id="kt_chat_messenger_footer">
                            <form class="form_chat" action="{{ url('app/chat/send') }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="d-flex flex-stack">
                                    <div class="d-flex align-items-center me-2 w-100">
                                        <textarea name="body" id="body" class="form-control form-control-flush mb-3"
                                            rows="1" data-kt-element="input" placeholder="Kirim Pesan"></textarea>
                                    </div>
                                    <button class="btn btn-primary btn_send_chat btn-sm" type="submit">Kirim</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script src="{{ asset('') }}js/app/chat/index.js"></script>
@endpush
